#!/usr/bin/perl -w

sub fib {
        my $pos = $_[0];

        if($pos < 0) {
                return 0;
        }
        elsif($pos <= 1) {
                return $pos;
        }

        return fib($pos - 2) + fib($pos - 1);
}

# set the process name so we can find it in the syslog output
$0 = "fib.pl";

$error = 0;
if($#ARGV < 0) {
        $error = 1;
}
elsif($ARGV[0] < 0) {
        $error = 1;
}

if($error) {
        print STDERR "usage: $0 POSITIVE_INTEGER\n";
        exit 1;
}

my $pos_arg = $ARGV[0];
fib($pos_arg);
