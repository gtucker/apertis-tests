#!/bin/sh

# This runs a CPU-intensive program while running Sysprof. If 

# XXX: keep this synchronized with the XSLT file's procName
TEST_DIR=$(dirname $0)
TEST_PROG=${TEST_DIR}/fib.pl
# calculate the Fibonacci sequence to position 40. Runtime increases
# exponentially by this number
TEST_PROG_ARGS="36"
SYSPROF_OUT_FILE="sysprof-out.xml"
XSLT_FILE="${TEST_DIR}/total-count.xsl"
MIN_PORTION="0.95"

die() {
        echo $1
        exit 0
}

#
# This test has to be run as root for sysprof.
#
if [ "`whoami`" != "root" ]; then
        exec sudo -E $0
fi

#
# Clean-up from previous runs
#
rm -f $SYSPROF_OUT_FILE

#
# Main test
#

sysprof-cli $SYSPROF_OUT_FILE &
SYSPROF_PID=$!

$TEST_PROG $TEST_PROG_ARGS 2>&1
# terminate sysprof to have it write its output file
kill $SYSPROF_PID

kill_result=$?
if [ $kill_result -ne 0 ]; then
        echo "sdk-performance-tools-sysprof-smoke-test: FAILED"
        echo 2>&1 "ERROR: core test program finished before sysprof started."
        exit 0
fi

# ensure that sysprof has finished writing its file out before we parse it
wait $SYSPROF_PID

TEST_PROG_PORTION=$(xsltproc --nonet --novalid $XSLT_FILE $SYSPROF_OUT_FILE)
# Convert each to a percentage and drop any fractional part (so we can compare
# with [)
TEST_PCT=$(echo $TEST_PROG_PORTION 100 | awk '{print int($1*$2)}')
MIN_PCT=$(echo $MIN_PORTION 100 | awk '{print int($1*$2)}')

TEST_PCT_LC=$(echo $TEST_PCT | tr '[:upper:]' '[:lower:]')
# handle division by zero in case of catastrophic XSLT failure
if [ "x$TEST_PCT_LC" = "xnan" ]; then
        TEST_PCT=0
fi

STATUS_STR="FAILED"
if [ $TEST_PCT -ge $MIN_PCT ]; then
        STATUS_STR="PASSED"
fi

echo "sdk-performance-tools-sysprof-smoke-test: $STATUS_STR"
echo "Program ran in $TEST_PCT% of samples (>= $MIN_PCT% required)"

if [ "$STATUS_STR" = "FAILED" ]; then
        echo "Contents of $SYSPROF_OUT_FILE:"
        echo ""
        cat $SYSPROF_OUT_FILE
        echo ""
        echo "Samples by each process:"
        xsltproc --nonet --novalid ${TEST_DIR}/sysprof-usage.xsl $SYSPROF_OUT_FILE | sort -n -r
fi
