all:

pkglibdir = /usr/lib/apertis-tests

SUBDIRS = \
	apparmor/goals \
	apparmor/dbus \
	apparmor/folks \
	apparmor/geoclue \
	apparmor/gstreamer1.0 \
	apparmor/tracker \
	apparmor/tumbler \
	apparmor/ofono \
	apparmor/pulseaudio \
	bluez/ \
	boot-performance/automated/ \
	cgroups/automated/ \
	clutter-bigger-reactive-area/ \
	clutter-i18n/manual/ \
	clutter-rotate/ \
	clutter-transparent-stage/ \
	clutter-zoom/ \
	common/ \
	dbus/ \
	firewall/automated/ \
	geoclue/automated/ \
	gettext/automated/ \
	glib/automated/ \
	gstreamer-buffering/automated/ \
	gupnp/automated/ \
	librest/automated/ \
	libsoup/automated/ \
	networking/proxy-manual/ \
	sqlite/automated/ \
	traffic-control/manual/ \
	sdk/automated/ \
	connman \
	contacts \
	gstreamer-viv-direct-texture/automated/ \
	telepathy \
	rfkill \
	$(NULL)

# Bits that are architecture-independent and can just be copied
COPY = \
	$(wildcard apparmor/*.sh) \
	$(wildcard apparmor/*.yaml) \
	apertis_tests_lib \
	apparmor/automated \
	apparmor/folks \
	apparmor/run-aa-test \
	common \
	dbus \
	firewall \
	folks \
	grilo \
	gstreamer-decode \
	inherit-config.sh \
	misc \
	predeployed-misc \
	resources \
	sdk \
	templates \
	tracker \
	tumbler \
	$(NULL)

all:
	for path in $(SUBDIRS); do \
		if test -x $$path/autogen.sh; then \
			( cd $$path && ./autogen.sh --prefix=/usr ) || exit $$?; \
		fi; \
		$(MAKE) -C $$path all || exit $$?; \
	done

$(patsubst %,install-%,$(COPY)): install-%:
	install -d $(DESTDIR)$(dir $(pkglibdir)/$*)
	cp -a $* $(DESTDIR)$(dir $(pkglibdir)/$*)

install: $(patsubst %,install-%,$(COPY))
	install -d $(DESTDIR)$(pkglibdir)
	install -d $(DESTDIR)$(pkglibdir)/apparmor
	install -d $(DESTDIR)/usr/lib/chaiwala-apparmor-tests
	for path in $(SUBDIRS); do $(MAKE) -C $$path install || exit $$?; done

clean:
	for path in $(SUBDIRS); do $(MAKE) -C $$path clean || exit $$?; done

check:
	misc/syntax-check.py
