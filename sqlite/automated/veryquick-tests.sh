#!/bin/sh
# vim: set sts=4 sw=4 et tw=0 :

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

#########
# Setup #
#########
trap "setup_failure" EXIT

: ${SQLITE_TESTDIR:=/usr/share/sqlite3/tests}
: ${SQLITE_TESTFIXTURE:=/usr/lib/sqlite3/testfixture}

SQLITE_TEMP_TESTDIR=$(create_temp_workdir)
src_copy "${SQLITE_TESTDIR}" "${SQLITE_TEMP_TESTDIR}"

if [ ! -x ${SQLITE_TESTFIXTURE} ]; then
    cry "Unable to find sqlite testfixture!"
    setup_failure
fi

setup_success
###########
# Execute #
###########
trap "test_failure" EXIT

cd "${SQLITE_TEMP_TESTDIR}"
# The oserror test opens 2000 files to check whether sqlite can handle EMFILE
# However, the fd limit is much higher than that, so we artificially reduce it
ulimit -n 1024
"${SQLITE_TESTFIXTURE}" "tests/veryquick.test"

test_success
