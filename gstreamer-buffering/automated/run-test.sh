#!/bin/sh
# vim: set sts=4 sw=4 et tw=0 :

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

TEST_MEDIA_DIR="${TEST_MEDIA_DIR:-/usr/share/chaiwala-test-media}"
VIDEO_PLAYER="${VIDEO_PLAYER:-${TESTLIBDIR}/video-player}"

# Running HTTP server used for network cgroup test
HTTP_SERVER_PID=""

# Port number for local HTTP server
HTTP_SERVER_PORT=""

# List of processes to kill on exit
PROC_KILL_LIST=

# Kill all pids in the list. It's okay if the list has stale pids.
# Theoretically, OSes recycle pids, but that's over a long period of time.
# We're unlikely to have collisions in the (relatively) short span of this test.
_kill_procs() {
    local proc
    echo "Killing processes..."
    for proc in ${PROC_KILL_LIST}; do
        echo -n "Killing $proc... "
        if ps -p $proc >/dev/null; then
            kill -9 $proc
            echo "Done!"
        else
            echo "Not needed."
        fi
    done
    echo "Finished killing processes."
}

_setup_qdiscs() {
    # Delete any old stuff, if needed
    _wipe_qdiscs || true
    # Rate-limit loopback so we have some buffering happening
    sudo tc qdisc add dev lo root handle 1: netem rate 0.6mbit
}

_setup_http_server() {
    local www_root=$1

    HTTPD_LOG_FILE=`mktemp`
    echo "simple-httpd log file:" $HTTPD_LOG_FILE
    cd "${www_root}" || setup_failure
    /usr/lib/libsoup2.4/simple-httpd > ${HTTPD_LOG_FILE} &
    # Running HTTP servers used for network cgroup test
    HTTP_SERVER_PID=$!

    HTTP_SERVER_PORT=$(tail -f ${HTTPD_LOG_FILE} --pid ${HTTP_SERVER_PID} | grep -m1 "Listening" | cut -d ':' -f 3 | sed "s/\///g")
}

_wipe_qdiscs() {
    sudo tc qdisc delete dev lo root
}

_cleanup() {
    kill -9 ${HTTP_SERVER_PID}
    if [ -n "$HTTPD_LOG_FILE" ]; then
        rm $HTTPD_LOG_FILE
    fi
    _wipe_qdiscs
}

#########
# Setup #
#########
trap "setup_failure" EXIT

# Video player executable
if [ ! -x "${VIDEO_PLAYER}" ]; then
    cry "Unable to find video-player executable!"
    setup_failure
fi

# Need root to setup and use cgroups
check_not_root
ensure_dbus_session
if ! pulseaudio --check; then
    cry "Pulseaudio isn't running!"
    setup_failure
fi

setup_success

# used the display the simple-httpd logs if the test failed to help debugging
display_httpd_log_file() {
    if [ -n "$HTTPD_LOG_FILE" ]; then
        echo "------------- simple-httpd logs -------------"
        cat $HTTPD_LOG_FILE
        echo "---------------------------------------------"
    fi
}

###########
# Execute #
###########
test_gstreamer_download_buffering() {
    local ret server_pid
    local media_file

    ## Setup ##
    _setup_qdiscs

    echo "Test setup complete, beginning test..."

    ## Test ##
    _setup_http_server "${TEST_MEDIA_DIR}"
    ret=$?

    # Check it is running
    if ! kill -0 ${HTTP_SERVER_PID}; then
        ret=1
    fi

    if [ $ret -gt 0 ]; then
        echo "Unable to start the http server properly!"
        display_httpd_log_file
        _cleanup
        return $ret
    fi

    media_file="http://localhost:${HTTP_SERVER_PORT}/trailer_400p.ogg"
    cd "${TESTLIBDIR}"
    ${VIDEO_PLAYER} "${media_file}"
    ret=$?

    if [ $ret -gt 0 ]; then
        display_httpd_log_file
        echo "Test failed!"
    else
        echo "Test passed!"
    fi

    ## Cleanup ##
    _cleanup
    return $ret
}

cleanup_and_maybe_fail () {
    s=$?
    _run_cmd _cleanup
    [ $s -eq 0 ] || test_failure
}
trap "cleanup_and_maybe_fail" EXIT

src_test_pass <<-EOF
test_gstreamer_download_buffering
EOF

test_success
