#!/bin/sh

echo "# $0: running as: $(id)"
echo "# $0: running in: $(pwd)"
echo "# $0: initial environment:"
env | LC_ALL=C sort | sed -e 's/^/#  /'

# Debuggability
set -x

cur_dir=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)

mkdir -p ~/.local/share/folks

# Make sure there are no existing telepathy accounts.
$cur_dir/remove-tp-accounts.sh 

# Create test111 account
$cur_dir/create-account.sh 1 

sleep 2

output=`folks-inspect individuals 2>&1 | grep gabble.echo`
alloutput=`folks-inspect individuals`

$cur_dir/remove-tp-accounts.sh

if test "z$output" = "z"; then
  echo "folks-retrieve-contacts-telepathy: FAILED"
  echo "folks-inspect individuals output is $alloutput"
  exit 1
else
  echo "folks-retrieve-contacts-telepathy: PASSED"
  exit 0
fi

