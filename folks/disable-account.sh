#!/bin/sh

# Debuggability
set -x

cur_dir=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)

if test "$1" = "1"; then
id=`mc-tool list | grep test111`
mc-tool disable $id
mc-tool auto-connect $id off

elif test "$1" = "2"; then
id=`mc-tool list | grep test222`
mc-tool disable $id
mc-tool auto-connect $id off

elif test "$1" = "3"; then
id=`mc-tool list | grep test333`
mc-tool disable $id
mc-tool auto-connect $id off

elif test "$1" = "4"; then
id=`mc-tool list | grep test444`
mc-tool disable $id
mc-tool auto-connect $id off
fi
