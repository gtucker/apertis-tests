#!/bin/sh

echo "# $0: running as: $(id)"
echo "# $0: running in: $(pwd)"
echo "# $0: initial environment:"
env | LC_ALL=C sort | sed -e 's/^/#  /'

set -x

export FOLKS_DEBUG_NO_COLOUR=1
export G_MESSAGES_DEBUG=all

exec /usr/lib/chaiwala-tests/contacts/folks-extended-info
