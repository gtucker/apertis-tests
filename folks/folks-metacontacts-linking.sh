#!/bin/sh

echo "# $0: running as: $(id)"
echo "# $0: running in: $(pwd)"
echo "# $0: initial environment:"
env | LC_ALL=C sort | sed -e 's/^/#  /'

# Debuggability
set -x

cur_dir=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)

# Clean up to make sure we have a good test.
$cur_dir/remove-tp-accounts.sh
# Remove all contacts from eds.
syncevolution --delete-items backend=evolution-contacts @foo bar '*'

# Start test.
# Create test111 account.
$cur_dir/create-account.sh 1

# Add one Local 1 contact into eds.
syncevolution --import $cur_dir/local1.vcf backend=evolution-contacts @foo bar

TMPFILE=`tempfile`
folks-inspect personas > ${TMPFILE} 2>&1
eds_uid=`cat ${TMPFILE} | grep "uid.*eds" | awk '{ print $2 }'`
test333_uid=`cat ${TMPFILE} | grep "uid.*test333" | awk '{ print $2 }'`
test444_uid=`cat ${TMPFILE} | grep "uid.*test444" | awk '{ print $2 }'`
rm ${TMPFILE}

# Link Local 1 to test333 and test444
# sleep here because on "fast" platforms there is a race and these
# calls fail with: Failed to add contact for persona store ID
# 'eds:system-address-book': Unknown error adding contact: Conflicting
# UIDs found in added contacts
sleep 1
folks-inspect linking link-personas $eds_uid $test333_uid
sleep 1
folks-inspect linking link-personas $eds_uid $test444_uid

# Ensure "Local 1" is found in folks.
output=`folks-inspect individuals 2>&1 | grep "jabber.*test333.*test444"`

# Clean up after the test.
$cur_dir/remove-tp-accounts.sh
# Clean up the eds addressbook.
syncevolution --delete-items backend=evolution-contacts @foo bar '*'

if test "z$output" != "z"; then
  echo "folks-metacontacts-linking: PASSED"
  exit 0
else
  echo "folks-metacontacts-linking: FAILED"
  exit 1
fi

