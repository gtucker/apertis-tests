#!/bin/sh

# Debuggability
set -x

cur_dir=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)

for a in `mc-tool list`; do
  mc-tool remove $a
done
