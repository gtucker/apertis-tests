#!/bin/sh

echo "# $0: running as: $(id)"
echo "# $0: running in: $(pwd)"
echo "# $0: initial environment:"
env | LC_ALL=C sort | sed -e 's/^/#  /'

# Debuggability
set -x
set -e

BACKUP_FILES=""
PROCESSES_TO_KILL=""

cleanup () {
    if test -n "$BACKUP_FILES"; then
        for f in $BACKUP_FILES; do
            sudo mv $f.bak $f
        done
    fi

    if test -n "$PROCESSES_TO_KILL"; then
        for p in $PROCESSES_TO_KILL; do
            sudo pkill -9 $p
        done
    fi
}

die () {
    cleanup
    echo "folks-sync-only: FAILED"

    exit 1
}

backup_file () {
    if [ ! -f $1.bak ]; then
	sudo cp $1 $1.bak || die
    fi
    BACKUP_FILES="$BACKUP_FILES $1"
}

# Setup ofono and ofono-phonesim
sudo systemctl stop ofono || die

backup_file /etc/ofono/phonesim.conf
printf "[phonesim]\nAddress=127.0.0.1\nPort=12345\n" | sudo tee /etc/ofono/phonesim.conf > /dev/null

sudo systemctl start ofono || die

# Start ofono-phonesim
sudo ofono-phonesim -p 12345 /usr/share/phonesim/default.xml 2>&1 | sed -e 's/^/# /' &
PROCESSES_TO_KILL="$PROCESSES_TO_KILL ofono-phonesim"

modem=`/usr/share/ofono/tests/list-modems | grep phonesim | cut -d' ' -f2`
if test -z "$modem"; then
    echo "# No modem available"
    die
fi

# Power on the modem
sleep 3
/usr/share/ofono/tests/enable-modem $modem 2>&1 | sed -e 's/^/# /'
/usr/share/ofono/tests/online-modem $modem 2>&1 | sed -e 's/^/# /'
sleep 3

G_MESSAGES_DEBUG=all FOLKS_DEBUG_NO_COLOUR=1 /usr/lib/chaiwala-tests/contacts/folks-sync-only 2>&1 | sed -e 's/^/# /'
cleanup
