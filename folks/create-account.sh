#!/bin/sh

# Debuggability
set -x

cur_dir=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)

if test "$1" = "1"; then
mc-tool add gabble/jabber Test111 string:account=test111@test.collabora.co.uk \
        string:password='-111test-' bool:ignore-ssl-errors=true > /dev/null 2>&1
id=`mc-tool list | grep test111`
mc-tool enable $id
mc-tool auto-connect $id on

elif test "$1" = "2"; then
mc-tool add gabble/jabber Test222 string:account=test222@test.collabora.co.uk \
        string:password='-222test-' bool:ignore-ssl-errors=true > /dev/null 2>&1
id=`mc-tool list | grep test222`
mc-tool enable $id
mc-tool auto-connect $id on

elif test "$1" = "3"; then
mc-tool add gabble/jabber Test333 string:account=test333@test.collabora.co.uk \
        string:password='-333test-' bool:ignore-ssl-errors=true > /dev/null 2>&1
id=`mc-tool list | grep test333`
mc-tool enable $id
mc-tool auto-connect $id on

elif test "$1" = "4"; then
mc-tool add gabble/jabber Test444 string:account=test444@test.collabora.co.uk \
        string:password='-444test-' bool:ignore-ssl-errors=true > /dev/null 2>&1
id=`mc-tool list | grep test444`
mc-tool enable $id
mc-tool auto-connect $id on
fi
