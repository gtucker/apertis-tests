#! /usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright © 2015 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import unittest
import os
import sys

from gi.repository import GLib
from gi.repository import Gio
from gi.repository import Grl

# import from toplevel directory
sys.path.insert(0, os.path.join(os.path.dirname(__file__),
                                os.pardir, os.pardir))
from apertis_tests_lib.tracker import TrackerIndexerMixin
from apertis_tests_lib.grilo import GriloBrowserMixin
from apertis_tests_lib.tumbler import TumblerMixin
from apertis_tests_lib import ApertisTest
from apertis_tests_lib import LONG_JPEG_NAME


class TrackerTest(ApertisTest, TrackerIndexerMixin, GriloBrowserMixin,
                  TumblerMixin):
    def __init__(self, *args, **kwargs):
        ApertisTest.__init__(self, *args, **kwargs)
        TrackerIndexerMixin.__init__(self)
        GriloBrowserMixin.__init__(self)
        TumblerMixin.__init__(self)

    def setUp(self):
        self.copy_medias(self.homedir)

    def tearDown(self):
        # Keep everything in place for further manual checks
        pass

    def media_files_tests(self):
        print("TrackerTest: media files tests")
        files = [self.homedir + '/Documents/lorem_presentation.odp',
                 self.homedir + '/Documents/lorem_spreadsheet.ods',
                 self.homedir + '/Documents/more_lorem_ipsum.odt',
                 self.homedir + '/Pictures/' + LONG_JPEG_NAME,
                 self.homedir + '/Pictures/collabora-logo-big.png',
                 self.homedir + '/Videos/big_buck_bunny_smaller.ogv',
                 '/home/shared/Music/Dee_Yan-Key_-_Lockung.ogg']
        for f in files:
            self.assertTrue(os.path.isfile(f), "Missing media file: " + f)

    def tracker_config_tests(self):
        print("TrackerTest: config tests")
        settings = Gio.Settings.new("org.freedesktop.Tracker.Miner.Files")
        self.assertEqual(settings.get_boolean('index-removable-devices'), True)
        self.assertEqual(settings.get_int('removable-days-threshold'), 60)
        self.assertEqual(settings.get_int('initial-sleep'), 0)
        self.assertEqual(settings.get_string('sched-idle'), 'never')
        self.assertEqual(settings.get_boolean('enable-monitors'), True)
        # self.assertEqual(settings.get_boolean('enable-writeback'), False)
        indexed = sorted(settings.get_strv('index-recursive-directories'))
        expected = ['&DOCUMENTS', '&MUSIC', '&PICTURES', '&VIDEOS',
                    '/home/shared/Music', '/home/shared/Pictures',
                    '/home/shared/Videos']
        self.assertEqual(indexed, expected)
        self.assertEqual(settings.get_strv('index-single-directories'), [])

    def tracker_journal_tests(self):
        print("TrackerTest: journal tests")
        path = ('%s/.local/share/tracker/data/tracker-store.journal' %
                self.homedir)
        self.assertFalse(os.path.isfile(path))

    def tracker_initial_tests(self):
        print("TrackerTest: initial tests")
        self.assert_all_indexed(self.homedir)

    def tracker_update_tests(self):
        print("TrackerTest: update tests")

        # Create a new file and assert it gets indexed
        filename = self.homedir + '/Documents/something.txt'
        with open(filename, 'w') as f:
            f.write('something')
        self.wait(True)
        self.assert_indexed(filename, '?urn nie:plainTextContent "something"')

        # Modify the file should re-index it
        with open(filename, 'w') as f:
            f.write('something else')
        self.wait(True)
        self.assert_indexed(
            filename, '?urn nie:plainTextContent "something else"')

        # Delete file and assert it's not indexed anymore
        os.remove(filename)
        self.wait(False)
        self.assert_not_indexed(filename)

    def assert_has_thumbnail(self, filename):
        self.tumbler_assert_thumbnailed(self.homedir, filename)

    def thumbnail_tests(self):
        print("TrackerTest: thumbnail tests")

        self.tumbler_drain_queue()

        self.assert_has_thumbnail('Documents/lorem_presentation.odp')
        self.assert_has_thumbnail('Documents/lorem_spreadsheet.ods')
        self.assert_has_thumbnail('Documents/more_lorem_ipsum.odt')
        self.assert_has_thumbnail('Pictures/' + LONG_JPEG_NAME)
        self.assert_has_thumbnail('Pictures/collabora-logo-big.png')
        self.assert_has_thumbnail('Videos/big_buck_bunny_smaller.ogv')

        # Make sure it thumbnailed shared medias as well. This particular OGG
        # was known to have issues for thumbnailing.
        self.tumbler_assert_thumbnailed('/home/shared',
                                        'Music/Dee_Yan-Key_-_Lockung.ogg')

    def grl_assert_common(self, medias, filename):
        self.assertTrue(filename in medias)
        media = medias[filename]

        # 'file:///foo/generic.mp3' -> 'generic.mp3'
        url = media.get_url()
        title = url[url.rfind('/') + 1:]
        self.assertEqual(media.get_title(), title)

    def grl_assert_music(self, medias, filename):
        self.assertTrue(filename in medias)
        media = medias[filename]

        self.assertTrue(isinstance(media, Grl.MediaAudio))
        self.assertEqual(media.get_title(), 'GNOME Generic Sound')
        self.assertEqual(media.get_artist(), 'Conrad Parker')
        self.assertEqual(media.get_album(), 'GNOME Audio')

    def grl_source_added_cb(self, registry, source):
        medias = self.browse(source, self.homedir)
        self.grl_assert_music(medias, 'Music/generic.mp3')
        self.grl_assert_music(medias, 'Music/generic.flac')
        self.grl_assert_music(medias, 'Music/generic-no-artwork.mp3')
        self.grl_assert_music(medias, 'Music/generic.oga')
        self.grl_assert_common(medias, 'Music/generic.wav')
        self.grl_assert_common(medias, 'Pictures/' + LONG_JPEG_NAME)
        self.grl_assert_common(medias, 'Pictures/collabora-logo-big.png')
        self.grl_assert_common(medias, 'Videos/big_buck_bunny_smaller.ogv')

        self.loop.quit()

    def tracker_grilo_tests(self):
        print("TrackerTest: grilo tests")
        registry = Grl.Registry.get_default()
        registry.load_plugin_by_id('grl-tracker')
        registry.connect('source-added', self.grl_source_added_cb)
        self.loop.run()

    # This is the only test case to make only one setup of tracker
    def test_all(self):
        self.media_files_tests()
        self.tracker_config_tests()
        self.tracker_journal_tests()
        self.start()
        self.tracker_initial_tests()
        self.tracker_update_tests()
        self.tracker_grilo_tests()
        self.thumbnail_tests()

if __name__ == "__main__":
    Grl.init([])
    unittest.main()
