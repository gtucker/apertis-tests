#!/bin/sh
# vim: set sts=4 sw=4 et tw=0 :

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

#########
# Setup #
#########
trap "setup_failure" EXIT

setup_success

###########
# Execute #
###########
start_light_server() {
    local light_server="${LIBEXECDIR}/gupnp/light-server"
    LIGHT_SERVER_LOG="${WORKDIR}/light-server.log"

    # Run the server used for the tests
    ${light_server} > "${LIGHT_SERVER_LOG}" &
    LIGHT_SERVER_PID=$!
}

cleanup_light_server() {
    if kill -s 0 ${LIGHT_SERVER_PID}; then
        kill -KILL ${LIGHT_SERVER_PID}
    fi
}

match_output() {
    if [ "${1}" != "${2}" ]; then
        echo $1
        echo $2
        return 1
    fi
}

test_light_server_client() {
    local expected_output="The light is now off.
The light is now on.
The light is now off.
The light is now on."
    local light_client="${LIBEXECDIR}/gupnp/light-client"
    ${light_client} on >/dev/null
    _sleep 2
    ${light_client} off >/dev/null
    _sleep 2
    ${light_client} toggle >/dev/null
    _sleep 2
    match_output "${expected_output}" "$(cat "${LIGHT_SERVER_LOG}")"
}

test_service_browsing() {
    local test_log="${WORKDIR}/test-browsing.log"
    "${LIBEXECDIR}/gupnp/test-browsing" > "${test_log}" &
    _sleep 5
    kill -INT %%
    # Was the test successful?
    for pattern in "urn:schemas-upnp-org:service:SwitchPower:1" \
        "urn:schemas-upnp-org:device:BinaryLight:1"; do
        grep -q -e "$pattern" "${test_log}" || return 1
    done
}

test_service_introspection() {
    local test_log="${WORKDIR}/test-introspection.log"
    "${LIBEXECDIR}/gupnp/test-introspection" > "${test_log}" &
    _sleep 5
    kill -INT %%
    # Was the test successful?
    for pattern in "name: SetTarget" "name: NewTargetValue" \
        "name: GetTarget" "name: RetTargetValue" \
        "name: GetStatus" "name: ResultStatus" \
        "name: Target" "type: gboolean" "name: Status"; do
        grep -q -e "$pattern" "${test_log}" || return 1
    done
}

cleanup_and_maybe_fail () {
    s=$?
    cleanup_light_server
    [ $s -eq 0 ] || test_failure
}
trap "cleanup_and_maybe_fail" EXIT

start_light_server

src_test_pass <<-EOF
test_light_server_client
test_service_browsing
test_service_introspection
EOF

cleanup_light_server

test_success
