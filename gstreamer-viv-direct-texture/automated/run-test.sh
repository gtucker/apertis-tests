#!/bin/sh
# vim: set sts=4 sw=4 et :

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

TEST_MEDIA_DIR="${TEST_MEDIA_DIR:-/usr/share/chaiwala-test-media}"
VIDEO_PLAYER="${VIDEO_PLAYER:-${TESTLIBDIR}/cogl-basic-video-player}"
TIME="${TIME:-/usr/bin/time}"

#########
# Setup #
#########
trap "setup_failure" EXIT

if [ ! -x "${VIDEO_PLAYER}" ]; then
    cry "Unable to find cogl-basic-video-player executable!"
    setup_failure
fi

if [ "$(uname -m)" != "armv7l" ]; then
    say "Test disabled on non-arm architectures"
    exit 0
fi

TEMP_FILE=$(mktemp)

setup_success

###########
# Execute #
###########
_cleanup() {
    rm -f "${TEMP_FILE}"
}

cleanup_and_maybe_fail () {
    s=$?
    _cleanup
    [ $s -eq 0 ] || test_failure
}
trap "cleanup_and_maybe_fail" EXIT

${TIME} -f %e -o ${TEMP_FILE} ${VIDEO_PLAYER} "file://${TEST_MEDIA_DIR}/bbb_1080p_30fps_cut.mp4"

duration=$(tail -n1 ${TEMP_FILE})
say_n "'gstreamer_viv_direct_texture' test took: ${duration} seconds ... "

# If playback takes more than 61 seconds, test failed
# The video duration is 60 seconds, so it should take at most this
# time to play (plus setup time), as coglsink is running with sync=false
if echo  "${duration} > 61" | bc -l | grep 1 >/dev/null;
then
    echo_red "FAIL\n"
    test_failure
else
    echo_green "PASS\n"
    test_success
fi
