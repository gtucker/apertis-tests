/* vim: set sts=2 sw=2 et :
 *
 * This program listens for cgroup events for memory, and prints an event when
 * the given threshold is crossed in either direction.
 *
 * It doesn't do error checking
 *
 */

#define _GNU_SOURCE
#include <sys/eventfd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>

#define handle_error(msg) \
  do { perror(msg); exit(EXIT_FAILURE); } while (0)

#define CGROUP_PATH	    "/sys/fs/cgroup/memory/"

int get_memory_usage_file_fd (char *cgroup_name)
{
  int ret;
  int memory_usage_fd;
  char *memory_usage_file;

  ret = asprintf (&memory_usage_file,
		  CGROUP_PATH "%s/memory.usage_in_bytes", cgroup_name);
  if (ret == -1)
    handle_error ("asprintf");

  memory_usage_fd = open (memory_usage_file, O_RDONLY);
  if (memory_usage_fd == -1)
    handle_error ("open");

  free (memory_usage_file);
  return memory_usage_fd;
}

void subscribe_to_cgroup_events (int   listener_fd,
				 int   memory_usage_fd,
				 char *memory_threshold,
				 char *memory_cgroup_name)
{
  char *event_control_file;
  char *line;
  int event_control_fd;
  int ret;

  ret = asprintf (&event_control_file,
		  CGROUP_PATH "%s/cgroup.event_control", memory_cgroup_name);
  if (ret == -1)
    handle_error ("asprintf");

  event_control_fd = open (event_control_file, O_WRONLY);
  if (event_control_fd == -1)
    handle_error ("open");

  /* Generate the string to be written for subscribing to notifications */
  ret = asprintf (&line, "%d %d %s",
		  listener_fd, memory_usage_fd, memory_threshold);
  if (ret == -1)
    handle_error ("asprintf");

  /* Subscribe to memory usage threshold notifications */
  if (write (event_control_fd, line, strlen(line) + 1) == -1)
    handle_error ("write");

  free (event_control_file);
  free (line);
}

int main (int   argc,
	  char *argv[])
{
  int ret;
  int listener_fd;
  char *memory_threshold;
  char *memory_cgroup_name;
  uint64_t event;

  if (argc != 3) {
    printf ("Usage: %s <memory cgroup name> <memory threshold>\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  memory_cgroup_name = argv[1];
  memory_threshold = argv[2];

  listener_fd = eventfd (0, 0);
  if (listener_fd == -1)
    handle_error ("eventfd");

  subscribe_to_cgroup_events (listener_fd,
			      get_memory_usage_file_fd (memory_cgroup_name),
			      memory_threshold,
			      memory_cgroup_name);

  printf ("Waiting for memory threshold notification...\n");
  ret = read (listener_fd, &event, sizeof(event));
  if (ret == -1)
    handle_error ("read");

  printf ("Threshold crossed!\n");
  exit(EXIT_SUCCESS);
}
