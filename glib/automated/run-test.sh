#!/bin/sh
# vim: set sts=4 sw=4 et :

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

#########
# Setup #
#########
trap "setup_failure" EXIT

: ${GLIB_TEST_DIR:=$(echo "${LIBEXECDIR}"/glib2.0/installed-tests/glib)}

if [ ! -d "${GLIB_TEST_DIR}" ]; then
    cry "Unable to find GLib tests dir!"
    setup_failure
fi

# We only run the tests related to disk I/O 
GLIB_TEST_LIST="async-close-output-stream
buffered-input-stream
buffered-output-stream
converter-stream
data-input-stream
data-output-stream
file
fileattributematcher
filter-streams
g-file
g-file-info
io-stream
live-g-file
pollable
readwrite
unix-streams
vfs
"

setup_success

###########
# Execute #
###########
trap "test_failure" EXIT

for each in ${GLIB_TEST_LIST}; do
    echo "${GLIB_TEST_DIR}/$each"
done | src_test_pass

test_success
