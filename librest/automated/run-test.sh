#!/bin/sh
# vim: set sts=4 sw=4 et tw=0 :

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

#########
# Setup #
#########
trap "setup_failure" EXIT

# oauth,oauth-sync tests need dbus, and the others try to spawn it
ensure_dbus_session

setup_success

###########
# Execute #
###########
trap "test_failure" EXIT

# The 'tr' below assumes there's no spaces in the path
for d in ${LIBEXECDIR}/librest/custom-* ${LIBEXECDIR}/librest/flickr \
         ${LIBEXECDIR}/librest/lastfm   ${LIBEXECDIR}/librest/oauth* \
         ${LIBEXECDIR}/librest/proxy*   ${LIBEXECDIR}/librest/threaded
do
    echo "$d" | \
        tr ' ' '\n' | check_file_exists_tee | src_test_pass
done

# These are expected to fail (as per upstream)
echo "${LIBEXECDIR}/librest/xml" | check_file_exists_tee | src_test_fail

test_success
