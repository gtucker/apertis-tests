#!/bin/sh

set -e
set -x
e=0

cat /proc/$$/attr/current

# Test <abstractions/folks> in malicious ways
# Here we test that an attacker cannot access files we mean not to be accessible

#NOTE: the profile for this script will allow /bin/touch and the other binaries
# as well as the abstractions/consoles

# This should not be accessible and auditd should complain
if touch $HOME/_file_which_do_not_exist_; then
    echo "that should not have worked"
    e=1
fi

# This should not be accessible and auditd should complain
if cat $HOME/.bash_history; then
    echo "that should not have worked"
    e=1
fi

# This should not be accessible and auditd should complain
if rm -f $HOME/.bash_history; then
    echo "that should not have worked"
    e=1
fi

exit $e
