#!/bin/sh
# vim: set sts=4 sw=4 et tw=0 :

set -e

TEST_DIR="$(cd "$(dirname "$0")" && pwd)"

if [ $# -ne 0 ]; then
    echo "Usage: $0"
    exit 1
fi

${TEST_DIR}/../../folks/folks-metacontacts-linking.sh
