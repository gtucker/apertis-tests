#!/usr/bin/python3

import os
import sys
import unittest

from gi.repository import Gio, GLib

class TestDBusAppArmor(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestDBusAppArmor, self).__init__(*args, **kwargs)

        self.bus = Gio.bus_get_sync(Gio.BusType.SESSION, None)

        self.proxy = Gio.DBusProxy.new_sync(self.bus,
                Gio.DBusProxyFlags.NONE, None,
                'org.Test', '/org/Test', 'org.Test', None)

        print('%s: AppArmor context: %s' % (sys.argv[0], open('/proc/self/attr/current').read()))

    def test_accept(self):
        ret = self.proxy.Accept('(s)', 'Accept')
        self.assertEqual(ret, "Accept called.")

    def test_audit_and_accept(self):
        ret = self.proxy.AuditAndAccept('(s)', 'AuditAndAccept')
        self.assertEqual(ret, "AuditAndAccept called.")

    def test_deny(self):
        with self.assertRaises(GLib.GError) as catcher:
            self.fail(repr(self.proxy.Deny('(s)', 'Deny')))

        e = catcher.exception
        self.assertEqual(e.domain, "g-dbus-error-quark")
        self.assertRegexpMatches(e.message, r'^GDBus\.Error:org\.freedesktop\.DBus\.Error\.AccessDenied')

    def test_audit_and_deny(self):
        with self.assertRaises(GLib.GError) as catcher:
            self.fail(repr(self.proxy.AuditAndDeny('(s)', 'AuditAndDeny')))

        e = catcher.exception
        self.assertEqual(e.domain, "g-dbus-error-quark")
        self.assertRegexpMatches(e.message, r'^GDBus\.Error:org\.freedesktop\.DBus\.Error\.AccessDenied')

if __name__ == '__main__':
    unittest.main(verbosity=2, buffer=True)
