#include <tunables/global>

/usr/lib/apertis-tests/apparmor/dbus/dbus-client.py {
  #include <abstractions/dbus>
  #include <abstractions/dbus-session-strict>
  #include <abstractions/gnome>
  #include <abstractions/python>

  /usr/bin/client.py rmPx,

  # let us read our own /proc for debugging
  owner @{PROC}/@{pid}/attr/current r,
  ptrace (read) peer=@{profile_name},

  dbus (send)
       bus=session
       interface=org.Test
       path=/org/Test
       member=Accept,

  audit dbus (send)
       bus=session
       interface=org.Test
       path=/org/Test
       member=AuditAndAccept,

  deny dbus (send)
       bus=session
       interface=org.Test
       path=/org/Test
       member=Deny,

  audit deny dbus (send)
       bus=session
       interface=org.Test
       path=/org/Test
       member=AuditAndDeny,
}
