#include <tunables/global>

/usr/lib/apertis-tests/apparmor/dbus/dbus-server.py {
  #include <abstractions/dbus-strict>
  #include <abstractions/dbus-session-strict>
  #include <abstractions/gnome>
  #include <abstractions/python>

  /usr/lib/apertis-tests/apparmor/dbus/dbus-server.py rmPx,

  # let us read our own /proc for debugging
  owner @{PROC}/@{pid}/attr/current r,
  ptrace (read) peer=@{profile_name},

  dbus send
       bus=session
       path=/org/freedesktop/DBus
       interface=org.freedesktop.DBus
       member=RequestName
       peer=(name=org.freedesktop.DBus),

  dbus bind
       bus=session
       name=org.Test,

  dbus receive
       bus=session,
}

