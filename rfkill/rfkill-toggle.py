#!/usr/bin/python
import time
import sys
import subprocess

dis = subprocess.check_output("connmanctl disable bluetooth", stderr=subprocess.STDOUT, shell=True)

time.sleep(2)

#run the command twice to ensure we're getting a disabled result...

dis = subprocess.check_output("connmanctl disable bluetooth", stderr=subprocess.STDOUT, shell=True)
if dis != "Error bluetooth: Already disabled\n":
	print "rfkill-toggle: FAILED"
	print "Unable to disable bluetooth"
	sys.exit(1)

time.sleep(2)

try:
	rs = subprocess.check_output("sudo hciconfig hci0 reset", stderr=subprocess.STDOUT, shell=True)
except subprocess.CalledProcessError as e:
	rs = e.output
if rs != "Can't init device hci0: Operation not possible due to RF-kill (132)\n":
	print "rfkill-toggle: FAILED"
	print "RF-kill did not prevent device reset"
	sys.exit(1)

time.sleep(2)

dis = subprocess.check_output("connmanctl enable bluetooth", stderr=subprocess.STDOUT, shell=True)
if dis != "Enabled bluetooth\n":
	print "rfkill-toggle: FAILED"
	print "Unable to enable bluetooth"
	sys.exit(1)

time.sleep(2)

try:
	rs = subprocess.check_output("sudo hciconfig hci0 reset", stderr=subprocess.STDOUT, shell=True)
except subprocess.CalledProcessError as e:
	rs = e.output
if rs != "":
	print "rfkill-toggle: FAILED"
	print "Device reset failed when it should succeed"
	sys.exit(1)

print "rfkill-toggle: PASSED"
sys.exit(0)
