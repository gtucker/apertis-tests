#!/bin/sh
# vim: set sts=4 sw=4 et :

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

#########
# Setup #
#########
trap "setup_failure" EXIT

setup_success

###########
# Execute #
###########
trap "test_failure" EXIT

# Not all the executables are automated tests
# The 'tr' below assumes there's no spaces in the path
# The 'sed' prepends tests which are expected to be skipped with '#';
# check_file_exists_tee must be called first
for d in ${LIBEXECDIR}/libsoup2.4/installed-tests/libsoup/*-test \
         ${LIBEXECDIR}/libsoup2.4/installed-tests/libsoup/header-parsing \
         ${LIBEXECDIR}/libsoup2.4/installed-tests/libsoup/date \
         ${LIBEXECDIR}/libsoup2.4/installed-tests/libsoup/uri-parsing
do
    echo "$d" | \
        tr ' ' '\n' | \
        check_file_exists_tee | \
        sed -e '/\/\(proxy-test\|xmlrpc-test\)$/ s/^/# /' | \
        src_test_pass
done

test_success
